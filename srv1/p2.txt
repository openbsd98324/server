
# $ less /etc/rc.conf
hostname="generic"
ifconfig_DEFAULT="DHCP"
sshd_enable="YES"
sendmail_enable="NONE"
sendmail_submit_enable="NO"
sendmail_outbound_enable="NO"
sendmail_msp_queue_enable="NO"
growfs_enable="YES"

fsck_y_enable="YES"
ifconfig_ue0="inet 10.0.0.10 netmask 0xffffff00"
ifconfig_ue1="inet 192.168.10.7 netmask 0xffffff00"
defaultrouter="10.0.0.238"

